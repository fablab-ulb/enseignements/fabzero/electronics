+++
title = "Specifications"
weight = 10
+++

## Pin mapping

![](img/yd-rp2040.png)


## Differences with Rpi Pico

- Switching mode power supply is replaced by a linear voltage regulator.
- VSYS is replaced by VIN. This pin can be used for external power (instead of USB) from 4,5v to 18v.
- VBUS is replaced by VOUT. This pin will supply 5V from USB or any higher voltage that is supplied to VIN.
- 3V3 EN pin is replaced by GP23, it is connected to the RGB LED data bus.
- GP24 is not used for power sense, it is connected to the USR (user) push-button.
- 4MB of flash memory instead of 2MB.
- RST (reset) push-button to restart program without disconnecting USB.

## Schematic diagram

[Schematic diagram in PDF](pdf/yd-rp2040_schematic.pdf)
