+++
title = "Kit contents"
weight = 10
+++

## Hardware and modules

- RP2040 development board
- USB-C cable
- Breadboard
- Push-button
- Jumper wires female-female
- Jumper wires male-female
- Jumper wires male-male
- [DHT20 temperature & RH sensor](https://www.tinytronics.nl/shop/en/sensors/air/humidity/asair-dht20-temperature-and-humidity-sensor-i2c)
- [Rotary encoder module](https://www.tinytronics.nl/shop/en/switches/manual-switches/rotary-encoders/rotary-encoder-module)

## Passive components

- [10k potentiometer](https://www.tinytronics.nl/shop/en/components/resistors/potentiometers/10k%CF%89-cermet-potmeter-type-3386p)
- 150Ω resistors
- 1k resistors
- 2k resistors
- 10k resistors
- [Light sensitive resistor](https://www.tinytronics.nl/shop/en/sensors/optical/light-and-color/gl5528-ldr-light-sensitive-resistor)
- [Piezoelectric buzzer](https://be.farnell.com/en-BE/tdk/ps1240p02bt/piezoelectric-buzzer-4khz-70dba/dp/3267212)

## Semiconductors

- [Zener diodes 3.3V](https://be.farnell.com/en-BE/multicomp/1n4728a/diode-zener-1w-3-3v-do-41/dp/1861443)
- [Optocoupler](https://be.farnell.com/en-BE/isocom/sfh615a-3x/optocoupler-dip-4-tr-o-p/dp/1683349)
- [NPN transistor](https://befr.rs-online.com/web/p/bipolar-transistors/7619822)
- [N-Channel MOSFET](https://befr.rs-online.com/web/p/mosfets/1452042)
- [Orange LED](https://www.tinytronics.nl/shop/en/components/leds/leds/orange-led-3mm-bright)
