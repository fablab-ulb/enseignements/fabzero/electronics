+++
title = "Input & output devices"
weight = 10
+++

# Input devices

| Analog sensors                                                                            |
|---                          |---                                                          |
| Analog temperature sensor   | [VMA320](https://www.velleman.eu/products/view/?id=435554)  |
| Capacitive touch sensor     | [VMA305](https://www.velleman.eu/products/view/?id=435524)  |
| Hall effect sensor          | [OH49E](https://www.tinytronics.nl/shop/en/sensors/magnetic-field/oh49e-hall-effect-switch-ss49e-compatible)  |
| Heartbeat sensor            | [VMA340](https://www.velleman.eu/products/view/?id=450580)  |
| Soil moisture sensor        | [VMA303](https://www.velleman.eu/products/view/?id=435520)  |
| Water turbidity sensor      | [DFROBOT-SEN0189](https://www.tinytronics.nl/shop/en/sensors/liquid/dfrobot-gravity-analog-turbidity-sensor)  |
| UV light sensor             | [UVSENSORMOD](https://www.tinytronics.nl/shop/en/sensors/optical/light-and-color/uv-light-sensor-module-200-370nm)  |
| Sound detection module      | [VMA309](https://www.velleman.eu/products/view/?id=435532)  |
| Microphone module with adjustable amplifier | [MAX4466MICMOD](https://www.tinytronics.nl/shop/en/sensors/sound/max4466-microfoon-amplifier-module-with-microphone)  |
| MEMS gas sensor MiCS-5524   | [DFROBOT-SEN0440](https://www.tinytronics.nl/shop/en/sensors/air/gas/dfrobot-fermion-mems-gas-sensor-mics-5524) |
| Optical Dust sensor   | [GP2Y1010AU0F](https://www.tinytronics.nl/shop/en/sensors/air/dust/sharp-optical-dust-sensor-gp2y1010au0f) |

| Digital sensors                                                                                                                 |
|---                            |---                                                                                                |
| MEMS microphone               | [INMP441MIC](https://www.tinytronics.nl/shop/en/sensors/sound/inmp441-mems-microphone-i2s)        |
| CO2 sensor                    | [MF-MTP40-F](https://www.tinytronics.nl/shop/en/sensors/air/gas/memsfrontier-mtp40-f-co2-sensor)  |
| TVOC and eCO2 sensor          | [SGP30MOD](https://www.tinytronics.nl/shop/en/sensors/air/gas/sgp30-tvoc-and-eco2-sensor-module)  |
| 11-ch. visible light sensor   | [DFROBOT-SEN0364](https://www.tinytronics.nl/shop/en/sensors/optical/light-and-color/dfrobot-gravity-as7341-11-channel-visible-light-sensor)  |
| 16-bit light sensor           | [BH1750FVI](https://www.tinytronics.nl/shop/en/sensors/optical/light-and-color/bh1750-16bit-digital-i2c-light-sensor-module)  |
| DC current and voltage sensor | [INA3221](https://www.tinytronics.nl/shop/en/sensors/current-voltage/ina3221-i2c-dc-current-and-voltage-sensor-module-1.6a-3-channels)  |
| Temperature sensor DS18b20    | [VMA324](https://www.velleman.eu/products/view/?id=439184)  |
| Magnetic angle sensor         | [AS5600MOD](https://www.tinytronics.nl/shop/en/sensors/magnetic-field/as5600-magnetic-angle-sensor-encoder-module)  |
| 3-axis accelerometer MMA8452  | [VMA208](https://www.velleman.eu/products/view/?id=439582)  |
| Accelerometer and gyroscope MPU-6500  | [MPU6500](https://www.tinytronics.nl/shop/en/sensors/acceleration-rotation/mpu-6500-accelerometer-gyroscope-6dof-module-3.3v-5v)  |
| Time-of-flight distance sensor VL53L0X  | [VMA337](https://www.velleman.eu/products/view/?id=450562)|
| Time-of-flight distance sensor VL6180   | [VL6180](https://www.tinytronics.nl/shop/en/sensors/distance/vl6180-time-of-flight-tof-distance-sensor) |
| Inductive proximity sensor    | [LJ8A3-2-Z/BX](https://www.tinytronics.nl/shop/en/3d-printing/electronics/accessories/lj8a3-2-z-bx-proximity-sensor-2mm-m8) |
| Light slot module             | [LIGHTSLOTSENSOR10MM](https://www.tinytronics.nl/shop/en/sensors/optical/light-slots/licht-slot-sensor-module-10mm)|
| Load cell amplifier HX711     | [HX711MOD](https://www.tinytronics.nl/shop/en/sensors/weight-pressure-force/load-cells/load-cell-amplifier-hx711) |

# Output devices

| Displays                                                                                              |
|---                                      |---                                                          |
| 4-digit LED display with TM1637 driver  | [VMA425](https://www.velleman.eu/products/view/?id=439208)  |
| 0.96" OLED display                      | [VMA438](https://www.velleman.eu/products/view/?id=439232)  |

| Power interface                                                                                       |
|---                                      |---                                                          |
| MOSFET module                           | [VMA411](https://www.velleman.eu/products/view/?id=435580)  |
| 5V Relay module                         | [VMA406](https://www.velleman.eu/products/view/?id=435570)  |
| Motor driver L298N                      | [VMA409](https://www.velleman.eu/products/view/?id=435576)  |
| Motor driver DRV8833                    | [DRV8833MOD](https://www.tinytronics.nl/shop/en/mechanics-and-actuators/motor-controllers-and-drivers/stepper-motor-controllers-and-drivers/drv8833-bipolar-stepper-motor-and-dc-motor-motor-controller)  |
| Stepper motor driver TMC2225            | [TMC2225DRIVERMODV1.1](https://www.tinytronics.nl/shop/en/mechanics-and-actuators/motor-controllers-and-drivers/stepper-motor-controllers-and-drivers/tmc2225-motor-driver-module-v1.1) |
| 2x3W stereo audio amplifier             | [PAM8403VC2](https://www.tinytronics.nl/shop/en/audio/amplifiers/2x3w-stereo-audio-amplifier-mini-5v-pam8403-volume-control-v2) |

| Electromechanics                        |
|---                                      |---|
| Servo motor   | [TS90D](https://www.tinytronics.nl/shop/en/mechanics-and-actuators/motors/servomotors/ts90d-mini-servo-1.6kg-continuous)  |
| Stepper motor | [BYJ48](https://www.tinytronics.nl/shop/en/mechanics-and-actuators/motors/stepper-motors/stepper-motor-with-uln2003-motor-controller)  |
| Electromagnet | [VMA431](https://www.velleman.eu/products/view/?id=439220)  |
