+++
title = "Precautions"
weight = 10
+++

## Output current limitation

By default, each GPIO is limited to a current of 4mA and can be programmed to draw up to 12mA.

The total input/output current of the device should not be higher than 50mA.

In order to prevent overload, input/output current must be constrained to these values.
For example, LED's can be protected with a resistor in series :  

[LED Series Resistor Calculator](https://www.digikey.be/en/resources/conversion-calculators/conversion-calculator-led-series-resistor)

## Over-voltage protection

The RP2040 GPIO's cannot withstand more than 3,3v. If required, you can build a circuit that will constrain input voltage to 3,3v.

Here are some examples that uses basic components included in the basic kit :

### Resistor divider bridge :

![](img/divider.png)

[Voltage Divider Calculator](https://www.digikey.be/en/resources/conversion-calculators/conversion-calculator-voltage-divider)

### Zener diode :

![](img/zener.png)

### Optocoupler :

![](img/optocoupler.png)

Source : [next-hack.com](https://next-hack.com/index.php/2017/09/15/how-to-interface-a-5v-output-to-a-3-3v-input/)
