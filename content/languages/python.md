+++
title = "Python"
weight = 20
+++

## Installation

- https://thonny.org/
- `pip install thonny`
- launch thonny
- tool > options > interpreter : rp2040
- reboot rp2040 in boot mode (boot button pressed while reset released)
- + click install or update MicroPython (bottom)
- ok => you're now talking/interpreting python from the rp2040
- select pico as target (pico and xiao-rp2040 act the same)
