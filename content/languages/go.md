+++
title = "Go"
weight = 30
+++

The RP2040 can be programmed using [TinyGo](https://tinygo.org/docs/reference/microcontrollers/pico/), a [Go](https://go.dev/) compiler for embedded system.

https://tinygo.org/docs/reference/microcontrollers/pico/

## Installation

### [Install Go](https://go.dev/doc/install)

#### For Linux

- [download Go](https://go.dev/dl/)
- `tar -xf go*.tar.gz # extract archive`
- `mv go ~ # put go folder to home`
- `echo 'PATH="~/go/bin:$PATH"' >> ~/.bashrc # add go to PATH through .bashrc`

### Install TinyGo

- Go to https://github.com/tinygo-org/tinygo/releases and get corresponding package OR https://tinygo.org/getting-started/install/ and follow instructions

```bash
wget https://github.com/tinygo-org/tinygo/releases/download/v0.26.0/tinygo_0.26.0_amd64.deb
sudo dpkg -i tinygo_0.26.0_amd64.deb
```

- Test everything's ok with `tinygo version`
- packages are stored in */usr/local/lib/tinygo/src*

## First test

```bash
mkdir -p ~/goTest/01-Test
cd ~/goTest/01-Test
go mod init blinky
```

blink.go file : 

```go
package main

import (
    "machine"
    "time"
)

func main() {
    led := machine.LED
    led.Configure(machine.PinConfig{Mode: machine.PinOutput})

    for {
        led.Low()
        time.Sleep(time.Second / 2)
        led.High()
        time.Sleep(time.Second / 2)
    }
}
```

- Build file using `tinygo build -target=targetname` (where targetname is one of the available targets in `tinygo targets`) : will create an elf file
- cand create other formats by giving output argument (extension will determine format) like `tinygo build -o 01_test.bin -target=pico` for example
- Flash it with `tinygo flash -target=pico` (will create )


## Using drivers

- `go get tinygo.org/x/drivers` inside a module (otherwise will lead to "go get" depreciation message => use go install etc. (just read the instructions))

## Try to compile the dev branch of tinyGo

sudo apt install cmake clang-13 llvm-13-dev lld-13 libclang-13-dev
- `sudo apt install cmake clang-14 llvm-14-dev lld-14 libclang-14-dev`
- `git clone https://github.com/tinygo-org/tinygo.git` (from https://github.com/tinygo-org/tinygo)
<!-- todo : check this -->
- `make llvm-source`
- `make llvm-build`
<!-- todo : continue -->

## Todo 

- (re)read article in num37 about raspberry pico
