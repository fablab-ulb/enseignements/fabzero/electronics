+++
title = "C / Arduino"
weight = 10
+++

## Install

What do you need? : 
- [Arduino](https://www.arduino.cc/en/software)
- [RP2040 board for Arduino](https://github.com/earlephilhower/arduino-pico#installing-via-arduino-boards-manager)

"Long story short" : 
- Install Arduino IDE
- Add `https://github.com/earlephilhower/arduino-pico/releases/download/global/package_rp2040_index.json` to the *Additional Board Manager* (in *Preferences*)
- Go to Tools->Boards->Board Manager in the IDE and install the "*pico*" (*Raspberry Pi Pico/RP2040*) package.

Here are other tutorial that can (possibly) be usefull : 
- https://www.instructables.com/Playing-Pico-W-With-Arduino-IDE/
- https://arduino-pico.readthedocs.io/en/latest/install.html
- https://wiki.seeedstudio.com/XIAO-RP2040-with-Arduino/

Alternatives :
- Arduino supports [Mbed OS core](https://blog.arduino.cc/2021/04/27/arduino-mbed-core-for-rp2040-boards/) for the RP2040 (available in boards manager)
