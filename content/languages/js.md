+++
title = "JavaScript"
weight = 40
+++

- https://kalumajs.org/

Blink example code : 

```javascript
// index.js
const led = 25;
pinMode(led, OUTPUT);
setInterval(() => {
  digitalToggle(led);
}, 1000);
```
