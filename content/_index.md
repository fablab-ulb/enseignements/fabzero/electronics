+++
title = "Electronics"
author = "Nicolas H.-P. De Coster (Vigon)"
+++

![](img/rp2040.png "The RP2040 processor from Raspberry Pi Ltd")

The module provided for this course (YD-RP2040) is an equivalent to the [Raspberry Pico](https://www.raspberrypi.com/products/raspberry-pi-pico/) with some additionnal features (Neopixel led and button).

## Useful documents

- [Rpi Pico Brief](https://datasheets.raspberrypi.com/pico/pico-product-brief.pdf)
- [Rpi Pico Datasheet](https://datasheets.raspberrypi.com/pico/pico-datasheet.pdf)
- [RP2040 Datasheet](https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf)

## Pinout

### YD-RP2040 (your module)

![](img/yd-rp2040.png)


### Rpi pico

![](img/pico_pinout.jpeg)


## Features

Particularly noteworthy features of the RP2040 (the core processor of your module) : 
- [ARM cortex-M0+](https://en.wikipedia.org/wiki/ARM_Cortex-M0%2B) 32bits, dual core
- 264k SRAM
- 133MHz native (250MHz+ easy overclock)
- 2 identical [PIO modules](https://en.wikipedia.org/wiki/Programmed_input%E2%80%93output), each with 4 state machines : so there are 8 state machines in total, numbered 0 to 7 : allows to implement almost any protocol in assembler, by unloading the cores from their management.
- Bootloader [**UF2**](https://github.com/microsoft/uf2) : modern UF2 bootloaders allow to get rid of any dedicated programmer by injecting directly the code (after reboot in bootmode) via the natively supported USB protocol. The target acts as a mass storage device on which to copy a simple precompiled uf2 binary file.
- For the other characteristics cfr wikipedia page or the [datasheet of the rp2040](https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf), particularly well written and human readable (rare!)

## [UF2 Bootloader](https://github.com/microsoft/uf2)

To program a microcontroller (inject code into memory), you usually need a dedicated programmer (special hardware). A bootloader is a small piece of code (program) pre-injected into the memory that allows user to communicate with the chip to inject a new program (via serial port, USB, ...).

UF2 :
- On-chip bootloader (no "programmer" needed) : 
- Talks through USB
- Developped by Microsoft (MakeCode) : https://makecode.com/blog/one-chip-to-flash-them-all

To enter the boot-mode (to program the chip), the "boot" button must be pressed when the chip starts : 
- Press the boot button while plugging the module into USB
- OR : press the `boot` button, press-then-release the `reset` button, so the `boot` button remains pressed while the chip restart.
- The module looks now like a "mass storage" ("usb-key") to the os and you can copy your code to program the chip. (or do it via your programming interface)
