+++
title = "DHT20 - Temperature & RH sensor"
weight = 10
+++

## Exercice

- Read the [sensor's datasheet](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf) and indentify relevant characteristics
- Connect the sensor to your development board
- Write a program to read data and send it to your computer over usb
- Write a program to process data and display some informations using the onboard LED

## Examples and libraries

- [MicroPython library](https://github.com/flrrth/pico-dht20)
- [C examples for I2C devices](https://github.com/raspberrypi/pico-examples/tree/master/i2c)
- [Arduino library](https://github.com/RobTillaart/DHT20)

To use the Arduino library examples with the RP2040, replace this line :

```c
DHT.begin();
```

with :

```c
Wire.setSDA(0);
Wire.setSCL(1);
Wire.begin();
```

This will setup I2C communication on pins 0 (SDA) and 1 (SCL).
