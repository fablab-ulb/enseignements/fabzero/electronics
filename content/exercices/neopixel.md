+++
title = "NeoPixel - Addressable RGB LED"
weight = 10
+++

## Exercice

- Write a program to assign a specific color to the RGB LED
- Try to do the same using a diferent programming language

## Examples and libraries

- [MicroPython example using PIO](https://github.com/raspberrypi/pico-micropython-examples/blob/master/pio/pio_ws2812.py)
- [C example using PIO](https://github.com/raspberrypi/pico-examples/tree/master/pio/ws2812)
- [Arduino library](https://github.com/adafruit/Adafruit_NeoPixel)
